package com.michalt.kieszonkowykardiolog.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.michalt.kieszonkowykardiolog.Model.SimpleMeasurement;
import com.michalt.kieszonkowykardiolog.Model.User;
import com.michalt.kieszonkowykardiolog.R;

public class AddSimpleMeasurementFragment extends Fragment implements View.OnClickListener {

    private TextView title;
    private TextView age;
    private EditText ageField;
    private TextView weight;
    private EditText weightField;
    private TextView height;
    private EditText heightField;
    private TextView highPressure;
    private EditText highPressureField;
    private TextView lowPressure;
    private EditText lowPressureField;
    private TextView heartRate;
    private EditText heartRateField;
    private Button saveButton;

    private double ageValue;
    private double weightValue;
    private double heightValue;
    private double highPressureValue;
    private double lowPressureValue;
    private double heartRateValue;
    private User user;
    private Callback callback;

    public AddSimpleMeasurementFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_simple_measurement, container, false);
        Bundle bundle = getArguments();
        if(bundle != null){
            user = bundle.getParcelable("user");
        }
        setupUI(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (Callback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;

    }

    public void setupUI(View view){
        title = (TextView) view.findViewById(R.id.titleSMID);
        age = (TextView) view.findViewById(R.id.ageSMID);
        ageField = (EditText) view.findViewById(R.id.ageSMFieldID);
        weight = (TextView) view.findViewById(R.id.weightSMID);
        weightField = (EditText) view.findViewById(R.id.weightSMFieldID);
        height = (TextView) view.findViewById(R.id.heightSMID);
        heightField = (EditText) view.findViewById(R.id.heightSMFieldID);
        highPressure = (TextView) view.findViewById(R.id.highPressureSMID);
        highPressureField = (EditText) view.findViewById(R.id.highPressureSMFieldID);
        lowPressure = (TextView) view.findViewById(R.id.lowPressureSMID);
        lowPressureField = (EditText) view.findViewById(R.id.lowPressureSMFieldID);
        heartRate = (TextView) view.findViewById(R.id.heartRateSMID);
        heartRateField = (EditText) view.findViewById(R.id.heartRateSMFieldID);
        saveButton = (Button) view.findViewById(R.id.saveButtonSMID);
        ageField.setText(Integer.valueOf(user.getAge()).toString());
        weightField.setText(Double.valueOf(user.getWeight()).toString());
        heightField.setText(Double.valueOf(user.getHeight()).toString());
        saveButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveButtonSMID:
                if (checkFieldRules()) {
                    getValuesFromFields();
                    SimpleMeasurement simpleMeasurement = new SimpleMeasurement(ageValue, user.getSex(), heartRateValue, highPressureValue, lowPressureValue, weightValue, heightValue);
                   // SimpleMeasurement simpleMeasurement = new SimpleMeasurement(ageValue,user.getSex(), heartRateValue,highPressureValue,lowPressureValue,weightValue,heightValue);
                    callback.onMeasureAdd(v,simpleMeasurement);
                }

                break;

        }
    }

    private boolean checkFieldRules() {
        boolean areSet = true;
        if (ageField.getText().toString().isEmpty()) {
            ageField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (weightField.getText().toString().isEmpty()) {
            weightField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (heightField.getText().toString().isEmpty()) {
            heightField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (highPressureField.getText().toString().isEmpty()) {
            highPressureField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (lowPressureField.getText().toString().isEmpty()) {
            lowPressureField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (heartRateField.getText().toString().isEmpty()) {
            heartRateField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        }
        return areSet;
    }

    private void getValuesFromFields(){
        ageValue = Double.parseDouble(ageField.getText().toString());
        weightValue = Double.parseDouble(weightField.getText().toString());
        heightValue = Double.parseDouble(heightField.getText().toString());
        highPressureValue = Double.parseDouble(highPressureField.getText().toString());
        lowPressureValue = Double.parseDouble(lowPressureField.getText().toString());
        heartRateValue = Double.parseDouble(heartRateField.getText().toString());
    }
    public interface Callback {
        public void onMeasureAdd(View v, SimpleMeasurement measurement );
    }


}
