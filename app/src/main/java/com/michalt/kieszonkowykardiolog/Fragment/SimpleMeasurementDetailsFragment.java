package com.michalt.kieszonkowykardiolog.Fragment;

import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.michalt.kieszonkowykardiolog.Model.SimpleMeasurement;
import com.michalt.kieszonkowykardiolog.R;

public class SimpleMeasurementDetailsFragment extends Fragment {

    private SimpleMeasurement measurement;

    private TextView date;
    private TextView age;
    private TextView weight;
    private TextView height;
    private TextView highPressure;
    private TextView lowPressure;
    private TextView heartRate;

    public SimpleMeasurementDetailsFragment() {



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simple_measurmeant_detalis, container, false);
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle != null){
            measurement = bundle.getParcelable("measurement");
        }
        setupUI(view );
        showData();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
    private void setupUI(View v) {
         date = v.findViewById(R.id.DateValueSimpleMeasurementDetailsID);
         age = v.findViewById(R.id.ageValueSimpleMeasurementDetailsID);
         weight = v.findViewById(R.id.weightValueSimpleMeasurementDetailsID);
         height = v.findViewById(R.id.heightValueSimpleMeasurementDetailsID);
         highPressure = v.findViewById(R.id.highPressureValueSimpleMeasurementDetailsID);
         lowPressure =v.findViewById(R.id.lowPressureValueSimpleMeasurementDetailsID);
         heartRate = v.findViewById(R.id.heartRateValueSimpleMeasurementDetailsID);
    }
    private void showData(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy_HH:mm");
        date.setText(dateFormat.format(measurement.getDateTime()));
         age.setText(String.valueOf(measurement.getAge()));
         weight.setText(String.valueOf(measurement.getWeight()));
         height.setText(String.valueOf(measurement.getHeight()));
         highPressure.setText(String.valueOf(measurement.getHighBloodPressure()));
         lowPressure.setText(String.valueOf(measurement.getThalach()));
         heartRate.setText(String.valueOf(measurement.getTrestbps()));
    }
}
