package com.michalt.kieszonkowykardiolog.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.michalt.kieszonkowykardiolog.Model.ExtendedMeasurement;
import com.michalt.kieszonkowykardiolog.Model.SimpleMeasurement;
import com.michalt.kieszonkowykardiolog.Model.User;
import com.michalt.kieszonkowykardiolog.R;


public class AddExtendedMeasurementFragment extends Fragment implements View.OnClickListener {

    private TextView title;
    private TextView age;
    private EditText ageField;
    private TextView weight;
    private EditText weightField;
    private TextView height;
    private EditText heightField;
    private TextView highPressure;
    private EditText highPressureField;
    private TextView lowPressure;
    private EditText lowPressureField;
    private TextView heartRate;
    private EditText heartRateField;
    private Button checkSaveButton;
    private TextView bloodSugar;
    private EditText bloodSugarField;
    private TextView cholesterol;
    private EditText cholesterolField;
    private TextView chestPainType;
    private Spinner chestPainTypeField;
    private TextView painType;
    private Spinner painTypeField;

    private double ageValue;
    private double weightValue;
    private double heightValue;
    private double highPressureValue;
    private double lowPressureValue;
    private double heartRateValue;
    private double bloodSugarValue;
    private double cholesterolValue;
    private double chestPainTypeValue=0;
    private double painTypeValue=-1;
    private User user;

    private Callback callback;

    public AddExtendedMeasurementFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_extended_measurement, container, false);
        Bundle bundle = getArguments();
        if(bundle != null){
            user = bundle.getParcelable("user");
        }
        setupUI(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (Callback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public void setupUI(View view){
        title = (TextView) view.findViewById(R.id.titleEMID);
        age = (TextView) view.findViewById(R.id.ageEMID);
        ageField = (EditText) view.findViewById(R.id.ageEMFieldID);
        weight = (TextView) view.findViewById(R.id.weightEMID);
        weightField = (EditText) view.findViewById(R.id.weightEMFieldID);
        height = (TextView) view.findViewById(R.id.heightEMID);
        heightField = (EditText) view.findViewById(R.id.heightEMFieldID);
        highPressure = (TextView) view.findViewById(R.id.highPressureEMID);
        highPressureField = (EditText) view.findViewById(R.id.highPressureEMFieldID);
        lowPressure = (TextView) view.findViewById(R.id.lowPressureEMID);
        lowPressureField = (EditText) view.findViewById(R.id.lowPressureEMFieldID);
        heartRate = (TextView) view.findViewById(R.id.heartRateEMID);
        heartRateField = (EditText) view.findViewById(R.id.heartRateEMFieldID);
        checkSaveButton = (Button) view.findViewById(R.id.checkSaveButtonEMID);
        checkSaveButton.setOnClickListener(this);
        bloodSugar = (TextView) view.findViewById(R.id.bloodSugarEMID);
        bloodSugarField = (EditText) view.findViewById(R.id.bloodSugarEMFieldID);
        cholesterol = (TextView) view.findViewById(R.id.cholesterolEMID);
        cholesterolField = (EditText) view.findViewById(R.id.cholesterolEMFieldID);
        chestPainType = (TextView) view.findViewById(R.id.chestPainTypeEMID);
        chestPainTypeField  = (Spinner) view.findViewById(R.id.spinnerChestPainTypeEMID);
        painType = (TextView) view.findViewById(R.id.painTypeEMID);
        painTypeField = (Spinner) view.findViewById(R.id.spinnerPainTypeEMID);
        ageField.setText(Integer.valueOf(user.getAge()).toString());
        weightField.setText(Double.valueOf(user.getWeight()).toString());
        heightField.setText(Double.valueOf(user.getHeight()).toString());
        setupChestPainTypeSpinner();
        setupPainTypeSpinner();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checkSaveButtonEMID:
                if (checkFieldRules()) {
                    getValuesFromFields();
                    ExtendedMeasurement extendedMeasurement = new ExtendedMeasurement(ageValue, user.getSex(), heartRateValue, highPressureValue, lowPressureValue, weightValue, heightValue, chestPainTypeValue, cholesterolValue, bloodSugarValue, painTypeValue);
                    callback.onMeasureAdd(v,extendedMeasurement);
                }

                break;

        }
    }

    private boolean checkFieldRules() {
        boolean areSet = true;
        if (ageField.getText().toString().isEmpty()) {
            ageField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (weightField.getText().toString().isEmpty()) {
            weightField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (heightField.getText().toString().isEmpty()) {
            heightField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (highPressureField.getText().toString().isEmpty()) {
            highPressureField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (lowPressureField.getText().toString().isEmpty()) {
            lowPressureField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        } if (heartRateField.getText().toString().isEmpty()) {
            heartRateField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        }
        if (bloodSugarField.getText().toString().isEmpty()) {
            bloodSugarField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        }
        if (cholesterolField.getText().toString().isEmpty()) {
            cholesterolField.setError(getText(R.string.emptyFieldNotification));
            areSet = false;
        }
        if (chestPainTypeValue == 0) {
            chestPainType.setTextColor(Color.RED);
            areSet = false;
        }else{chestPainType.setTextColor(Color.BLACK);}
        if (painTypeValue != 0) {
            painType.setTextColor(Color.RED);
            areSet = false;
        }else{painType.setTextColor(Color.BLACK); }
        return areSet;
    }

    private void getValuesFromFields(){
        ageValue = Double.parseDouble(ageField.getText().toString());
        weightValue = Double.parseDouble(weightField.getText().toString());
        heightValue = Double.parseDouble(heightField.getText().toString());
        highPressureValue = Double.parseDouble(highPressureField.getText().toString());
        lowPressureValue = Double.parseDouble(lowPressureField.getText().toString());
        heartRateValue = Double.parseDouble(heartRateField.getText().toString());
        bloodSugarValue = Double.parseDouble(bloodSugarField.getText().toString());
        cholesterolValue = Double.parseDouble(cholesterolField.getText().toString());
    }

    private void setupChestPainTypeSpinner() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.pain_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        chestPainTypeField.setAdapter(adapter);
        chestPainTypeField.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                chestPainTypeValue = position-1;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void setupPainTypeSpinner() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(), R.array.bool, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        painTypeField.setAdapter(adapter);
        painTypeField.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                painTypeValue = position-1;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    public interface Callback {
        public void onMeasureAdd(View v, SimpleMeasurement measurement );
    }

}
