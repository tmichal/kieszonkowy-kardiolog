package com.michalt.kieszonkowykardiolog.Fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.michalt.kieszonkowykardiolog.Activity.StatisticActivity;
import com.michalt.kieszonkowykardiolog.Model.HearthRatePlotData;
import com.michalt.kieszonkowykardiolog.Model.PressurePlotData;
import com.michalt.kieszonkowykardiolog.R;

import java.util.ArrayList;
import java.util.List;


public class HeartRatePlotFragment extends Fragment   {

    private LineChart hearthRatePlot;
    private List<Entry> hearthRateEntryList;
    private List<String> plotLabelsList;
    private List<HearthRatePlotData> hearthRatePlotDataList;


    public HeartRatePlotFragment() {
    }
    public  HeartRatePlotFragment(List<HearthRatePlotData> hearthRatePlotDataList) {
        this.hearthRatePlotDataList = hearthRatePlotDataList;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_heart_rate_plot, container, false);
        hearthRatePlot = view.findViewById(R.id.HeartRatePlotID);
        Bundle bundle = getArguments();
        if (bundle != null) {
            hearthRatePlotDataList = bundle.getParcelableArrayList("data");
        }else
            hearthRatePlotDataList =  new ArrayList<>();
        if(hearthRatePlotDataList.size()>=2)
        prepareData(hearthRatePlotDataList);
        else
        hearthRatePlot.setNoDataText("Za mało pomiarów do wyświetlenia");
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    private void setupData() {

        LineDataSet highPressureDataSet = new LineDataSet(hearthRateEntryList, this.getString(R.string.highPressureDataTittle));
        highPressureDataSet.setColor(Color.RED);
        highPressureDataSet.setDrawCircles(true);
        LineData data = new LineData(highPressureDataSet);
        data.setDrawValues(false);
        hearthRatePlot.setData(data);
        hearthRatePlot.setNoDataText("Brak danych");
    }

    private void setupAxisXY() {
        final String[] labels = (String[]) plotLabelsList.toArray(new String[0]);
        ValueFormatter formatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                return labels[(int) value];
            }
        };
        XAxis xAxis = hearthRatePlot.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);

        hearthRatePlot.getAxisRight().setEnabled(false);

        YAxis leftAxis = hearthRatePlot.getAxisLeft();
        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(120f);
        leftAxis.setLabelCount(12);


    }

    private void prepareData(List<HearthRatePlotData> hearthRatePlotDataList) {
        hearthRateEntryList = new ArrayList<>();
        plotLabelsList = new ArrayList<>();
        //plotLabelsList.add("");
        for (int i = 0; i < hearthRatePlotDataList.size(); i++) {
            HearthRatePlotData tmp = hearthRatePlotDataList.get(i);
            Entry tmpRate = new Entry(i, (float) tmp.getThalach());
            hearthRateEntryList.add(tmpRate);
            plotLabelsList.add(tmp.getDateString());
        }
       // plotLabelsList.add("");
        setupData();
        setupAxisXY();
        hearthRatePlot.invalidate();
    }

    public interface OnDataPass {
        public void onDataPass(String period);
    }

}
