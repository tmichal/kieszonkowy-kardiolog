package com.michalt.kieszonkowykardiolog.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.michalt.kieszonkowykardiolog.Activity.MeasurementDetailsActivity;
import com.michalt.kieszonkowykardiolog.Model.MyMeasurementData;
import com.michalt.kieszonkowykardiolog.R;

import java.util.List;

public class MyMeasurementsAdapter extends RecyclerView.Adapter<MyMeasurementsAdapter.ViewHolder> {
    private String username;
    private Context context;
    /**
     * Lista przechowująca infomację o obiektach które możemy wczytac z bazy danych
     */
    private List<MyMeasurementData> myMeasurementDataList;

    /**
     * Konsturktor klasy MyMeasurementsAdapter
     *
     * @param context            context
     * @param myMeasurementDataList Lista obiektów możliwych do wczytania z bazy danych
     */
    public MyMeasurementsAdapter(Context context, List myMeasurementDataList, String username) {
        this.context = context;
        this.myMeasurementDataList = myMeasurementDataList;
        this.username = username;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_single_measurmeant, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder( ViewHolder viewHolder, int position) {
        MyMeasurementData item = myMeasurementDataList.get(position);
        viewHolder.iD.setText(String.valueOf(item.getiD()));
        viewHolder.measurementName.setText(item.getMeasurementName());
        if(item.getMeasurementType().equals("simple"))
        viewHolder.measurementType.setText("prosty");
        else
        viewHolder.measurementType.setText("rozszerzony");
    }

    @Override
    public int getItemCount() {
        return myMeasurementDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        /**
         * ID każdego pomiaru
         */
        TextView iD;
        /**
         * Nazwa pomiaru
         */
        TextView measurementName;
        /**
         * Lokalizacja pomiaru
         */
        TextView measurementType;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            iD = itemView.findViewById(R.id.DataRowID);
            measurementName = itemView.findViewById(R.id.MeasurmeantNameTextViewID);
            measurementType = itemView.findViewById(R.id.MeasurmeantTypeTextViewID);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
             String selectedMeasurementName = myMeasurementDataList.get(position).getMeasurementName();
            String isSimple =  myMeasurementDataList.get(position).getMeasurementType();
            Intent intent = new Intent(v.getContext(), MeasurementDetailsActivity.class);
            intent.putExtra("username",username);
            intent.putExtra("measurement_name",selectedMeasurementName);
            intent.putExtra("is_simple", isSimple);
            v.getContext().startActivity(intent);
                    }
    }
}
