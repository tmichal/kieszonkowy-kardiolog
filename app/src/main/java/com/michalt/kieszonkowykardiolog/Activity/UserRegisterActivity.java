package com.michalt.kieszonkowykardiolog.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.michalt.kieszonkowykardiolog.R;
import com.michalt.kieszonkowykardiolog.Model.User;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class UserRegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private Button backButton;
    private Button createAccountButton;
    private EditText nameEditText;
    private EditText surnameEditText;
    private EditText ageEditText;
    private EditText password1EditText;
    private EditText password2EditText;
    private EditText emailEditText;
    private EditText weightEditText;
    private EditText heightEditText;
    private String sex;
    private Spinner sexChoiceSpinner;
    private Boolean accountCreated;
    private User user;
    private boolean creatingInProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        accountCreated = false;
        creatingInProgress = false;
        setupUI();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.RegisterButtonID: {
                creatingInProgress = true;
                createUserAccount();
                break;
            }
            case R.id.BackFromRegisterActivityButtonID: {
                backToMainActivity();
                break;
            }
        }

    }

    private void setupUI() {
        backButton = findViewById(R.id.BackFromRegisterActivityButtonID);
        createAccountButton = findViewById(R.id.RegisterButtonID);
        nameEditText = findViewById(R.id.NameFieldID);
        surnameEditText = findViewById(R.id.SurnameFieldID);
        ageEditText = findViewById(R.id.AgeFieldID);
        password1EditText = findViewById(R.id.NewPasswordField1ID);
        password2EditText = findViewById(R.id.NewPasswordField2ID);
        emailEditText = findViewById(R.id.EmailFieldID);
        weightEditText = findViewById(R.id.WeightFieldID);
        heightEditText = findViewById(R.id.HeightFieldID);
        setupSpinner();
        backButton.setOnClickListener(this);
        createAccountButton.setOnClickListener(this);

    }

    private void setupSpinner() {

        sexChoiceSpinner = (Spinner) findViewById(R.id.SexSpinnerID);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.sexArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sexChoiceSpinner.setAdapter(adapter);
        sexChoiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sex = sexChoiceSpinner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void createUserAccount() {
        if (checkFieldRules() && checkPasswordRules()) {
            String email = emailEditText.getText().toString();
            String password = password1EditText.getText().toString();
            String name = nameEditText.getText().toString();
            String surname = surnameEditText.getText().toString();
            double weight = Double.parseDouble(weightEditText.getText().toString());
            double height = Double.parseDouble(heightEditText.getText().toString());
            int age = Integer.valueOf(ageEditText.getText().toString());
            createAccount(email, password, name, surname, age, sex, weight, height);
            user = new User(name, surname, sex,email, age, weight,height);
        }
    }

    private boolean checkFieldRules() {
        boolean result = true;
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        String email = emailEditText.getText().toString();
        if (nameEditText.getText().toString().isEmpty()) {
            nameEditText.setError(getText(R.string.emptyFieldNotification));
            result = false;
        }
        if (surnameEditText.getText().toString().isEmpty()) {
            surnameEditText.setError(getText(R.string.emptyFieldNotification));
            result = false;
        }  if (ageEditText.getText().toString().isEmpty()) {
            ageEditText.setError(getText(R.string.emptyFieldNotification));
            result = false;
        } else if(Double.parseDouble(ageEditText.getText().toString())<20) {
            ageEditText.setError("Wiek nie możę być mniejszy niż 20 lat");
            result = false;
        }
        if (sex.isEmpty()) {
            Toast.makeText(UserRegisterActivity.this, R.string.emptySexField, Toast.LENGTH_SHORT).show();
            result = false;
        }  if (heightEditText.getText().toString().isEmpty() || heightEditText.getText().toString().equals(".")||heightEditText.getText().toString().equals(",")){
            heightEditText.setError(getText(R.string.emptyFieldNotification));
            result = false;
        }
         if (weightEditText.getText().toString().isEmpty() || weightEditText.getText().toString().equals(".")||weightEditText.getText().toString().equals(",")){
            weightEditText.setError(getText(R.string.emptyFieldNotification));
             result = false;
        }
        if(email.isEmpty()) {
            emailEditText.setError(getText(R.string.emptyFieldNotification));
            result = false;
        }
        else if(!pattern.matcher(email).matches())
        {
            emailEditText.setError("niepoprawny adres email");
            result = false;
        }
        return result;
    }

    private boolean checkPasswordRules() {
        String pass1 = password1EditText.getText().toString();
        String pass2 = password2EditText.getText().toString();
        if (!pass1.isEmpty() || !pass2.isEmpty()) {
            if (pass1.equals(pass2)) {
                if (pass1.length() >= 8) {
                    return true;
                } else {
                    Toast.makeText(UserRegisterActivity.this, R.string.passwordToShort, Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else {
                Toast.makeText(UserRegisterActivity.this, R.string.passwordNotMatch, Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {
            Toast.makeText(UserRegisterActivity.this, R.string.emptyPasswordField, Toast.LENGTH_SHORT).show();
            return false;

        }

    }

    private void saveUserData(final String email, final String name, final String  surname, final int age, final String sex,final double weight,final double height) {
        FirebaseFirestore databaseInstance = FirebaseFirestore.getInstance();
        Map<String, Object> userData = new HashMap<>();
        userData.put("surname", surname);
        userData.put("email", email);
        userData.put("name",name);
        userData.put("age",age);
        userData.put("sex",sex);
        userData.put("weight",weight);
        userData.put("height",height);

        databaseInstance.collection("users").document(email).set(userData).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(UserRegisterActivity.this, R.string.accountCreated, Toast.LENGTH_SHORT).show();
                creatingInProgress = false;
                accountCreated = true;
               backToMainActivity();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                creatingInProgress = false;
                Toast.makeText(UserRegisterActivity.this, R.string.acconutMotCreatedDataSaved ,Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        backToMainActivity();
    }

    private void createAccount(final String email, final String password, final String name, final String  surname, final int age, final String sex,final double weight,final double height) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseAuth.getInstance().signOut();
        firebaseAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(UserRegisterActivity.this, R.string.accountCreatedSavingData, Toast.LENGTH_SHORT).show();
                            saveUserData(email,name,surname,age,sex,weight,height);
                        } else {
                            Toast.makeText(UserRegisterActivity.this, R.string.accountNotCreated, Toast.LENGTH_SHORT).show();
                            creatingInProgress = false;
                        }
                    }
                });
    }

    private void backToMainActivity(){
        if( !creatingInProgress) {
            Intent returnIntent = getIntent();
            returnIntent.putExtra("accountCreated", accountCreated);
            if (accountCreated) {
                returnIntent.putExtra("user", user);
            }
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }

}

