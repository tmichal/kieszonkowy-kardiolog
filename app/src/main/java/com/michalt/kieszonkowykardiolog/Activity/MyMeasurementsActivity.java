package com.michalt.kieszonkowykardiolog.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.michalt.kieszonkowykardiolog.Adapter.MyMeasurementsAdapter;
import com.michalt.kieszonkowykardiolog.Model.MyMeasurementData;
import com.michalt.kieszonkowykardiolog.Model.User;
import com.michalt.kieszonkowykardiolog.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MyMeasurementsActivity extends AppCompatActivity implements View.OnClickListener {
    private List<MyMeasurementData> myMeasurementDataList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter myAdapter;
    private FirebaseFirestore databaseInstance;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_measurmeants);
        myMeasurementDataList = new ArrayList<>();
        recyclerView = findViewById(R.id.MyMeasurementsRecyclerViewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            user = bundle.getParcelable("user");
        }
        myAdapter = new MyMeasurementsAdapter(this, myMeasurementDataList,user.getEmail());
        recyclerView.setAdapter(myAdapter);
    }
    @Override
    public void onStart() {
        super.onStart();
        databaseInstance = FirebaseFirestore.getInstance();
        loadMeasurementsInfo();
    }
    @Override
    public void onClick(View v) {

    }
    private void sortMyMeasurementDataList(){
        myMeasurementDataList.sort((o1,o2) -> o2.getDate().compareTo(o1.getDate()));
        for(int j =0;j <myMeasurementDataList.size();j++){
            myMeasurementDataList.get(j).setiD(j+1);
        }

    }

    private void loadMeasurementsInfo() {
        myMeasurementDataList.clear();
        databaseInstance.collection("users_measurements").document(user.getEmail()).collection("measurements")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            int i = 1;
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                String type =document.getString("type");
                                Date date = document.getDate("date");
                                String name =document.getString("name");
                                MyMeasurementData tmp = new MyMeasurementData(name,type,date,i);
                                myMeasurementDataList.add(tmp);
                                i++;
                            }
                            sortMyMeasurementDataList();
                            myAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(MyMeasurementsActivity.this, R.string.downloadError, Toast.LENGTH_SHORT).show();
                        }
                        myAdapter.notifyDataSetChanged();
                    }
                });
    }

}
