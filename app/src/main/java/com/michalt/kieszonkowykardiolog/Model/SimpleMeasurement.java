package com.michalt.kieszonkowykardiolog.Model;

import android.icu.text.SimpleDateFormat;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Date;

public class SimpleMeasurement implements Parcelable {
    private String measurementName; // pomiar + Date
    private double age;
    private String sex;
    private double thalach; //heartRate
    private double highBloodPressure; //highBloodPressure
    private double trestbps; //lowbloodPreasure
    private Date dateTime;
    private double weight;
    private double height;

    public SimpleMeasurement(double age, String sex, double thalach, double highBloodPressure, double trestbps, double weight, double height) {
        this.age = age;
        this.sex = sex;
        this.thalach = thalach;
        this.highBloodPressure = highBloodPressure;
        this.trestbps = trestbps;
        this.weight = weight;
        this.height = height;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy_HH:mm");
        this.measurementName= "Pomiar "+ dateFormat.format(c.getTime());
        this.dateTime = c.getTime();
    }
        // potrzebny przy wczytywaniu z bazy
    public SimpleMeasurement(String measurementName, double age, String sex, double thalach, double highBloodPressure, double trestbps, Date dateTime, double weight, double height) {
        this.measurementName = measurementName;
        this.age = age;
        this.sex = sex;
        this.thalach = thalach;
        this.highBloodPressure = highBloodPressure;
        this.trestbps = trestbps;
        this.dateTime = dateTime;
        this.weight = weight;
        this.height = height;
    }

    public SimpleMeasurement() {

    }

    protected SimpleMeasurement(Parcel in) {
        measurementName = in.readString();
        age = in.readDouble();
        sex = in.readString();
        thalach = in.readDouble();
        highBloodPressure = in.readDouble();
        trestbps = in.readDouble();
        weight = in.readDouble();
        height = in.readDouble();
        dateTime = (Date)in.readSerializable();
    }

    public static final Creator<SimpleMeasurement> CREATOR = new Creator<SimpleMeasurement>() {
        @Override
        public SimpleMeasurement createFromParcel(Parcel in) {
            return new SimpleMeasurement(in);
        }

        @Override
        public SimpleMeasurement[] newArray(int size) {
            return new SimpleMeasurement[size];
        }
    };
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(measurementName);
        dest.writeDouble(age);
        dest.writeString(sex);
        dest.writeDouble(thalach);
        dest.writeDouble(highBloodPressure);
        dest.writeDouble(trestbps);
        dest.writeDouble(weight);
        dest.writeDouble(height);
        dest.writeSerializable(dateTime);
    }

    public double getAge() {
        return age;
    }

    public void setAge(int age) {

        this.age = age;
    }



    public String getSex() {
        return sex;
    }


    public void setSex(String sex) {
        this.sex = sex;
    }

    public double getThalach() {
        return thalach;
    }

    public void setThalach(double thalach) {
        this.thalach = thalach;
    }

    public double getHighBloodPressure() {
        return highBloodPressure;
    }

    public void setHighBloodPressure(int highBloodPleasure) {
        this.highBloodPressure = highBloodPleasure;
    }

    public double getTrestbps() {
        return trestbps;
    }

    public void setTrestbps(double trestbps) {
        this.trestbps = trestbps;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }


    public String getMeasurementName() {
        return measurementName;
    }

    public void setMeasurementName(String measurementName) {
        this.measurementName = measurementName;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }


}
