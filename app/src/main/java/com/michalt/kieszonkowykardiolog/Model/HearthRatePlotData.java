package com.michalt.kieszonkowykardiolog.Model;

import android.icu.text.SimpleDateFormat;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class HearthRatePlotData implements Parcelable {
    private double thalach;
    private Date date;
    private String dateString;

    public HearthRatePlotData() {
    }

    public HearthRatePlotData(double thalach, Date date) {
        this.thalach = thalach;
        this.date = date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy_HH:mm");
        this.dateString  = dateFormat.format(date);
    }

    protected HearthRatePlotData(Parcel in) {
        thalach = in.readDouble();
        dateString = in.readString();
        date = (Date) in.readSerializable();
    }

    public static final Creator<HearthRatePlotData> CREATOR = new Creator<HearthRatePlotData>() {
        @Override
        public HearthRatePlotData createFromParcel(Parcel in) {
            return new HearthRatePlotData(in);
        }

        @Override
        public HearthRatePlotData[] newArray(int size) {
            return new HearthRatePlotData[size];
        }
    };

    public double getThalach() {
        return thalach;
    }

    public void setThalach(double thalach) {
        this.thalach = thalach;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(thalach);
        dest.writeString(dateString);
        dest.writeSerializable(date);
    }
}
