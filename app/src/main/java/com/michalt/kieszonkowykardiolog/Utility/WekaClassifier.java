package com.michalt.kieszonkowykardiolog.Utility;

import android.content.res.AssetManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class WekaClassifier {

    private int attributesNumber;// = 9;//zwrocic uwage przy wywolaniu
    private int instancesNumber;// = 1;
    private Classifier classifier;
    private AssetManager assetManager;

    public WekaClassifier(int attributesNumber, int instancesNumber, AssetManager assetManager) {
        this.attributesNumber = attributesNumber;
        this.instancesNumber = instancesNumber;
        this.assetManager=assetManager;
        try {
            this.classifier = (Classifier) weka.core.SerializationHelper.read(assetManager.open("j48_4.model"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public double classify(int classIndex, double age, double sex, double cp, double trestbps, double chol, double fbs, double thalach, double exang) throws Exception {
        Instances dataSet = createInstances(classIndex);
        Instance instance = createInstance(age, sex, cp, trestbps, chol, fbs, thalach, exang);
        dataSet.add(instance);
        double  prediction = classifier.classifyInstance(dataSet.instance(0));
        Log.d("weka", String.valueOf(prediction));
        return prediction;
    }

    public Instances createInstances(int classIndex){
        List<Attribute> attributeList = new ArrayList<Attribute>();

        Attribute age = new Attribute("age"); //todo dac to w jakiejs konfiguracji?
        attributeList.add(age);
        Attribute sex = new Attribute("sex");
        attributeList.add(sex);
        Attribute cp = new Attribute("cp");
        attributeList.add(cp);
        Attribute trestbps = new Attribute("trestbps");
        attributeList.add(trestbps);
        Attribute chol = new Attribute("chol");
        attributeList.add(chol);
        Attribute fbs = new Attribute("fbs");
        attributeList.add(fbs);
        Attribute thalach = new Attribute("thalach");
        attributeList.add(thalach);
        Attribute exang = new Attribute("exang");
        attributeList.add(exang);
        Attribute classs = new Attribute("class",new ArrayList<String>(){{add("0");add("1");add("2");add("3");add("4");}});
        attributeList.add(classs);

        FastVector fvWekaAttributes = new FastVector(attributesNumber);
        for (int i = 0; i < (attributesNumber); ++i) {
            fvWekaAttributes.addElement(attributeList.get(i));
        }

        Instances dataSet = new Instances("InstancesDataSet", fvWekaAttributes, instancesNumber);
        dataSet.setClassIndex(classIndex);
        return dataSet;
    }

    public Instance createInstance(double age, double sex, double cp, double trestbps, double chol, double fbs, double thalach, double exang){
        Instance instance = new DenseInstance(attributesNumber);

        instance.setValue(0, age); //age
        instance.setValue(1, sex); //sex
        instance.setValue(2, cp); //cp
        instance.setValue(3, trestbps);//trestbps
        instance.setValue(4, chol); //chol
        instance.setValue(5, fbs); //fbs
        instance.setValue(7, thalach); //thalach
        instance.setValue(8,exang); //exang

        return instance;
    }


    public class WekaInstance {
        private double age;
        private double sex;
        private double cp;
        private double trestbps;
        private double chol;
        private double fbs;
//        private double restecg;
        private double thalach;
        private double exang;
//        private double oldpeak;
//        private double slope;
//        private double ca;
//        private double thal;

        public WekaInstance(double age, double sex, double cp, double trestbps, double chol, double fbs, double thalach, double exang) {
            this.age = age;
            this.sex = sex;
            this.cp = cp;
            this.trestbps = trestbps;
            this.chol = chol;
            this.fbs = fbs;
//            this.restecg = restecg;
            this.thalach = thalach;
            this.exang = exang;
//            this.oldpeak = oldpeak;
//            this.slope = slope;
//            this.ca = ca;
//            this.thal = thal;
        }


        public double getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public double getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public double getCp() {
            return cp;
        }

        public void setCp(int cp) {
            this.cp = cp;
        }

        public double getTrestbps() {
            return trestbps;
        }

        public void setTrestbps(int trestbps) {
            this.trestbps = trestbps;
        }

        public double getChol() {
            return chol;
        }

        public void setChol(int chol) {
            this.chol = chol;
        }

        public double getFbs() {
            return fbs;
        }

        public void setFbs(int fbs) {
            this.fbs = fbs;
        }

        public double getThalach() {
            return thalach;
        }

        public void setThalach(int thalach) {
            this.thalach = thalach;
        }

        public double getExang() {
            return exang;
        }

        public void setExang(int exang) {
            this.exang = exang;
        }


    }
}
